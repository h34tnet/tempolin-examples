package net.h34t

import WhitespaceFor
import WhitespaceIf
import kotlin.random.Random

fun main() {
    showBlocks()
    showConditional()
}

fun showBlocks() {
    WhitespaceFor(block = {
        generateSequence { Random.nextInt(256) }.filter { it % 2 == 0 }.take(10).sorted()
            .map { BlockBlock(it.toString()) }
    }).also { println(it) }
}

fun showConditional() {
    // should print "<div>\n    foo\n</div>\n"
    WhitespaceIf(isTrue = true).also { println(it) }

    // should print "<div>\n    bar\n</div>\n"
    WhitespaceIf(isTrue = false).also { println(it) }
}