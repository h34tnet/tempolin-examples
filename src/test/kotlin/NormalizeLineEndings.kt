private val nl = Regex("\r\n|\r|\n")

fun String.normalizeNL() = this.replace(nl, "\n")