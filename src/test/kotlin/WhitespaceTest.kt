import org.junit.jupiter.api.Test

class WhitespaceTest {

    @Test
    fun `test block whitespace`() {
        val str = WhitespaceFor(
            block = { sequenceOf("one", "two", "five").map { BlockBlock(it) } }
        ).toString()

        // this must be an exact whitespace match
        assert(
            str == """
            |<ul>
            |    <li>one</li>
            |    <li>two</li>
            |    <li>five</li>
            |</ul>
        """.trimMargin()
        )
    }

    @Test
    fun `test conditional whitespace`() {
        val str = WhitespaceIf(isTrue = true).toString()

        assert(
            str == """
            |<div>
            |    foo
            |</div>
            |
        """.trimMargin()
        )
    }
}