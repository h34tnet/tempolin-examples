plugins {
    kotlin("jvm") version "1.9.21"
    id("net.h34t.tempolin") version "0.9.4"
}

group = "net.h34t"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}
kotlin {
    jvmToolchain(21)
}

tasks.getByName("compileKotlin").dependsOn("tempolin")

tempolin {
    add {
        templateDirectory = "$projectDir/src/main/tpl"
        outputDirectory = "${layout.buildDirectory.get()}/generated-sources/tpl"
        addSources = true
    }
}
